# PDF generation
## Demonstrate creation of PDF from variables

### Steps to install

1. Clone repository  
`git clone https://hakanostrom@bitbucket.org/hakanostrom/hellopdf.git`

2. Install dependencies with bower  
`bower install`

3. Run in web browser
`index.html`

### Example
![alt text](output/akt_nr_23452.png)
![alt text](output/formulär.PNG)
